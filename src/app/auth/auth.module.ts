
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ForgetComponent } from './forget/forget.component';
 
import * as material from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [RegisterComponent, LoginComponent, ForgetComponent],
  exports: [LoginComponent,
    material.MatSelectModule,
    material.MatOptionModule,
    material.MatFormFieldModule,
    material.MatInputModule,
    material.MatToolbarModule,
    material.MatButtonModule,
    material.MatSidenavModule,
    material.MatIconModule,
    material.MatListModule,
    material.MatCardModule,
    material.MatGridListModule,

     
     ],
  imports: [
    CommonModule,
    RouterModule,
    material.MatSelectModule,
    material.MatOptionModule,
    material.MatFormFieldModule,
    material.MatInputModule,
    material.MatToolbarModule,
    material.MatButtonModule,
    material.MatSidenavModule,
    material.MatIconModule,
    material.MatListModule,
    material.MatCardModule,
    material.MatGridListModule,
    FormsModule,
    ReactiveFormsModule
    
     
  ]
})
export class AuthModule { }
