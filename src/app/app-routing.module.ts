import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component'
import { ForgetComponent } from './auth/forget/forget.component'
import { DashboardComponent } from './profile/dashboard/dashboard.component'

const routes: Routes = [
  {  path: '',  component: LoginComponent },
  {  path: 'register',  component: RegisterComponent },
  {  path: 'changepassword',  component: ForgetComponent },
  {  path: 'dashboard',  component: DashboardComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
