import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';

import * as material from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NavigationComponent } from '../navigation/navigation.component';
import { MatProgressBarModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [DashboardComponent,NavigationComponent,],
  exports: [DashboardComponent],
  imports: [
    CommonModule,
    RouterModule,
    material.MatSelectModule,
    material.MatOptionModule,
    material.MatFormFieldModule,
    material.MatInputModule,
    material.MatToolbarModule,
    material.MatButtonModule,
    material.MatSidenavModule,
    material.MatIconModule,
    material.MatListModule,
    material.MatCardModule,
    material.MatGridListModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressBarModule,
    FlexLayoutModule
    
  ]
})
export class ProfileModule { }
